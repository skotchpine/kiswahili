(ns kiswahili.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [clojure.string :as string]))

;;
;; ## DB
;;
(def phrases
  {"kuja"
   {"present"
    {"mimi"  [["" "Ninakuja" "lini?"] "When am I coming?"]
     "sisi"  [["" "Tunakuja" "lini?"] "When are we coming?"]
     "wewe"  [["" "Unakuja"  "lini?"] "When are you coming?"]
     "ninyi" [["" "Mnakuja"  "lini?"] "When are you all coming?"]
     "yeye"  [["" "Anakuja"  "lini?"] "When is he coming?"]
     "wao"   [["" "Wanakuja" "lini?"] "When are they coming?"]}
    
    "past"
    {"mimi"  [["" "Nilikuja" "lini?"] "When did I come?"]
     "sisi"  [["" "Tulikuja" "lini?"] "When did we come?"]
     "wewe"  [["" "Ulikuja"  "lini?"] "When did you come?"]
     "ninyi" [["" "Mlikuja"  "lini?"] "When did you all come?"]
     "yeye"  [["" "Alikuja"  "lini?"] "When did he come?"]
     "wao"   [["" "Walikuja" "lini?"] "When did they come?"]}
    
    "future"
    {"mimi"  [["" "Nitakuja" "lini?"] "When will I come?"]
     "sisi"  [["" "Tutakuja" "lini?"] "When will we come?"]
     "wewe"  [["" "Utakuja"  "lini?"] "When will you come?"]
     "ninyi" [["" "Mtakuja"  "lini?"] "When will you all come?"]
     "yeye"  [["" "Atakuja"  "lini?"] "When will he come?"]
     "wao"   [["" "Watakuja" "lini?"] "When will they come?"]}}
   
   "kuenda"
   {"present"
    {"mimi"  [["" "Ninaenda" "lini?"] "When am I going?"]
     "sisi"  [["" "Tunaenda" "lini?"] "When are we going?"]
     "wewe"  [["" "Unaenda"  "lini?"] "When are you going?"]
     "ninyi" [["" "Mnaenda"  "lini?"] "When are you all going?"]
     "yeye"  [["" "Anaenda"  "lini?"] "When is he going?"]
     "wao"   [["" "Wanaenda" "lini?"] "When are they going?"]}

    "past"
    {"mimi"  [["" "Nilienda" "lini?"] "When did I go?"]
     "sisi"  [["" "Tulienda" "lini?"] "When did we go?"]
     "wewe"  [["" "Ulienda"  "lini?"] "When did you go?"]
     "ninyi" [["" "Mlienda"  "lini?"] "When did you all go?"]
     "yeye"  [["" "Alienda"  "lini?"] "When did he go?"]
     "wao"   [["" "Walienda" "lini?"] "When did they go?"]}

    "future"
    {"mimi"  [["" "Nitaenda" "lini?"] "When will I go?"]
     "sisi"  [["" "Tutaenda" "lini?"] "When will we go?"]
     "wewe"  [["" "Utaenda"  "lini?"] "When will you go?"]
     "ninyi" [["" "Mtaenda"  "lini?"] "When will you all go?"]
     "yeye"  [["" "Ataenda"  "lini?"] "When will he go?"]
     "wao"   [["" "Wataenda" "lini?"] "When will they go?"]}}})

(def verbs (keys phrases))
(def tenses (-> phrases vals first keys))
(def subjects (-> phrases vals first vals first keys))

(def init
  {::verb 0
   ::tense 0
   ::subject 0})

;;
;; ## Events
;;
(re-frame/reg-event-fx ::init
  (fn [_ _]
    {:db init}))

(re-frame/reg-event-db ::next-verb
  (fn [{:keys [::verb] :as db} _]
    (let [i (inc verb)
          n (dec (count verbs))
          next-verb (if (> i n) 0 i)]
      (assoc db ::verb next-verb))))

(re-frame/reg-event-db ::prev-verb
  (fn [{:keys [::verb] :as db} _]
    (let [i (dec verb)
          n (dec (count verbs))
          prev-verb (if (< i 0) n i)]
      (assoc db ::verb prev-verb))))

(re-frame/reg-event-db ::next-tense
  (fn [{:keys [::tense] :as db} _]
    (let [i (inc tense)
          n (dec (count tenses))
          next-tense (if (> i n) 0 i)]
      (assoc db ::tense next-tense))))

(re-frame/reg-event-db ::prev-tense
  (fn [{:keys [::tense] :as db} _]
    (let [i (dec tense)
          n (dec (count tenses))
          prev-tense (if (< i 0) n i)]
      (assoc db ::tense prev-tense))))

(re-frame/reg-event-db ::next-subject
  (fn [{:keys [::subject] :as db} _]
    (let [i (inc subject)
          n (dec (count subjects))
          next-subject (if (> i n) 0 i)]
      (assoc db ::subject next-subject))))

(re-frame/reg-event-db ::prev-subject
  (fn [{:keys [::subject] :as db} _]
    (let [i (dec subject)
          n (dec (count subjects))
          prev-subject (if (< i 0) n i)]
      (assoc db ::subject prev-subject))))

;;
;; ## Subs
;;
(re-frame/reg-sub ::verb
  (fn [{:keys [::verb]} _]
    (nth verbs verb)))

(re-frame/reg-sub ::tense
  (fn [{:keys [::tense]} _]
    (nth tenses tense)))

(re-frame/reg-sub ::subject
  (fn [{:keys [::subject]} _]
    (nth subjects subject)))

(re-frame/reg-sub ::phrase
  :<- [::verb]
  :<- [::tense]
  :<- [::subject]
  (fn [[verb tense subject] _]
    (get-in phrases [verb tense subject])))

(re-frame/reg-sub ::swahili
  :<- [::phrase]
  (fn [[swahili-parts & _] _]
    (->> swahili-parts
         (remove empty?)
         (interpose " ")
         (apply str))))

(re-frame/reg-sub ::english
  :<- [::phrase]
  (fn [phrase _]
    (last phrase)))
                  
;;
;; ## Views
;;
(defn ArrowControl [{:keys [label
                             val-sub
                             next-sub
                             prev-sub]}]
  (let [val$ (re-frame/subscribe val-sub)]
    (fn []
      [:div.tc
       [:div
        [:div.h2.pt2
         [:span.pointer.ph2.f6.gold.fas.fa-chevron-left
          {:on-click #(re-frame/dispatch prev-sub)}]
         [:div.dib.f5 @val$]
         [:span.pointer.ph2.f6.gold.fas.fa-chevron-right
          {:on-click #(re-frame/dispatch next-sub)}]]
        [:div.f5.moon-gray label]]])))

(defn ControlPanel []
  (fn []
    ;; Header container
    [:div.w-100.bg-really-dark-gray.pa2.center.flex.items-center.justify-around.shadow-5

       ;; Max-width container
     [:div.w-100.mw6-ns.flex.items-center.justify-around

      ;; Control verb
      [ArrowControl
       {:label "verb"
        :val-sub [::verb]
        :prev-sub [::prev-verb]
        :next-sub [::next-verb]}]

      ;; Control tense
      [ArrowControl
       {:label "tense"
        :val-sub [::tense]
        :prev-sub [::prev-tense]
        :next-sub [::next-tense]}]

      ;; Control subject
      [ArrowControl
       {:label "subject"
        :val-sub [::subject]
        :prev-sub [::prev-subject]
        :next-sub [::next-subject]}]]]))

(defn English []
  (let [english (re-frame/subscribe [::english])]
    (fn []
      [:div.moon-gray.f5.ma2.mb0 @english])))

(defn Swahili []
  (let [swahili (re-frame/subscribe [::swahili])]
    (fn []
      [:div.f4.ma2.bg-near-black.pv4.ph2.br1.shadow-5.dib
       [:div.dib.f4.ph2.moon-gray.word-wrap @swahili]])))

(defn View []
  ;; Viewport container
  [:div.vw-100.vh-100.overflow-y-scroll.pb5
   [ControlPanel]

    ;; Max-width container
   [:div.w-100.mw8-ns.tc.center
    [English]
    [Swahili]]])

;;
;; ## Main
;;
(defn ^:export mount []
  (re-frame/dispatch-sync [::init])
  (enable-console-print!)
  (re-frame/clear-subscription-cache!)
  (js/console.log "Hello, there. ~ Skotchpine 2019")
  (->> "app"
       js/document.getElementById
       (reagent/render [View])))
